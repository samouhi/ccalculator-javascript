    var number1Input = document.getElementById('number1');
    var number2Input = document.getElementById('number2');
    var oppInput = document.getElementById('opp');
    var resultInput = document.getElementById('result');
    var numberOne = true;
    var numberTow = false;
    var pointNumber = true;
    var numberOneValue;
    function updateResult() {
     
        var number1 =number1Input.value;
        var number2 = number2Input.value ;
        var opp = oppInput.value;
        if (number2Input.value == "") {
            number2 = ""; 
        }
        var equation = number1 + opp + number2;
        resultInput.value = equation;
    }


       function finalResult() {
            var number1 = parseFloat(number1Input.value);
            var number2 = parseFloat(number2Input.value);
            var opp = oppInput.value;

           
            let result;
            if (opp == '+') {
                result = number1 + number2;
            } else if (opp == '-') {
                result = number1 - number2;
            } else if (opp == 'x') {
                result = number1 * number2;               
            } else if (opp == '÷') {
                result = number1 / number2;
            }

           resultInput.value = result;
           number1Input.value = result;
           numberOne = true;
           oppInput.value = "";
           number2Input.value = "";
                 
}

function enterNumber(x) {
    var number = x; 
    console.log(x);
    if (x == "." && pointNumber == false) {
        return 1;
    }
    if (x == ".") {
        pointNumber = false;        
    }

    if (numberOne) {
        number1Input.value += number; 
         console.log(number1Input.value);
        
    } else {
        console.log(number);
        number2Input.value += number;   
        console.log(number2Input);
    }
    updateResult();
}
        
function enterOperation(x) {
 
    if (number1Input.value!="") {
        oppInput.value = x;
        updateResult();
        numberOne = false;
        pointNumber = true;
    }
      
}

function DeleteAll() {
           resultInput.value = "";
           number1Input.value = "";
           oppInput.value = "";
           number2Input.value = "";
           numberOne = true;
}


function DeleteOneByOne() {

var number1InputValue = number1Input.value.toString();
var number2InputValue = number2Input.value.toString();
var oppInputValue = oppInput.value;

    if (number2Input.value != "") {
        number2Input.value = number2InputValue.substring(0, number2InputValue.length - 1);
        updateResult();
        return 1;

    }
  
    if (oppInput.value != "") {
        oppInput.value = oppInputValue.substring(0, oppInputValue.length - 1);
        updateResult();
        return 1;
    }
    if (number1Input.value != "") {
        number1Input.value = number1InputValue.substring(0, number1InputValue.length - 1);
        updateResult();
        return 1;
    }
}

    function precent() {
    var resultPrecnt;
    if (numberOne) { 
       resultPrecnt = number1Input.value / 100;
        number1Input.value = resultPrecnt;
        updateResult()
    } else {
        resultPrecnt = number2Input.value / 100;
         number2Input.value = resultPrecnt;
         updateResult()

    }
  
    
}
 
    function negativePostive() {
        var negativePostiveResult;
    if (numberOne) { 
      negativePostiveResult = number1Input.value *-1;
        number1Input.value = negativePostiveResult;
        updateResult()
    } else {
        negativePostiveResult = number2Input.value *-1;
         number2Input.value = negativePostiveResult;
         updateResult()

    }

 }
 
 